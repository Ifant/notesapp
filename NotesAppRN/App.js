import { StatusBar } from 'expo-status-bar';
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack'

import Home from './src/Home';
import TryPage from './src/TryPage';
import BlankNote from './src/blankNote';
import CreateList from './src/CreateList';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={Home}>
        <Stack.Screen name='Home' component={Home} options={{headerShown: false}}/>
        <Stack.Screen name='TryPage' component={TryPage} options={{headerShown: false}}/>
        <Stack.Screen name='CreateList' component={CreateList} options={{headerShown:false}}/>
        <Stack.Screen name='BlankNote' component={BlankNote} options={{headerShown:false}}/>
      </Stack.Navigator>
    </NavigationContainer>
    // <Home/> 
    // <TryPage/>
    // <BlankNote/>
    // <CreateList/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EB3B5A',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
