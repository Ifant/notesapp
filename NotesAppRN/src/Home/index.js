import React, { useEffect,useState } from 'react';
import {View, Text, StyleSheet, StatusBar, TouchableOpacity, FlatList} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { FontAwesome } from '@expo/vector-icons';
import { Feather } from '@expo/vector-icons';

let DATAS = [
    {title: '',contents: [{
        content:'',check:false
    }]}
]

export default function Home({route, navigation}){
    const [refresh,setRefresh] = useState();
    const [isThere, setIsThere] = useState(false);

    const getData = async()=> {
        try{
            let jsonValue = await AsyncStorage.getItem('allData');
            console.log('===ANDA MENDAPATKAN DATA DARI COMPONENT HOME!===');
            DATAS = JSON.parse(jsonValue);
            console.log(DATAS);
            console.log('===ANDA MENDAPATKAN DATA DARI COMPONENT HOME!====');
            setRefresh({});
        }
        catch(err){
            console(err);
        }
    }

    useEffect(()=> {
        getData();
        console.log('INI MASUK KE USE EFFECT!');

        navigation.addListener('focus', ()=> {
            console.log('Masuk ke listener');
            getData();
        })
    },[navigation])


    const deleteNote = async(index)=> {
        try{
            DATAS.splice(index,1);
            await AsyncStorage.setItem('allData', JSON.stringify(DATAS));
            setRefresh({})
        }catch(err){
            console.log(err)
        }
    }

    const editThis = (type,index) => {
        if (type === 'list'){
            console.log('==> Ini Index pada editThis Function, type list',index);
            navigation.push('CreateList',{indexTerpilih: index})
        }
        else {
            console.log('==> Ini Index pada editThis Function, type note',index);
            navigation.push('BlankNote',{indexTerpilih: index})
        }
    }

    const BoxNotes = (props)=>{
        console.log('==> Box Note Berjalan',props.index)
        return(
            <TouchableOpacity onPress={()=> editThis(props.type,props.index)} key={props.id} delayPressIn={0.2}>
                <View style={styles.boxNote}>
                    <Text style={styles.titleNote}>{props.title}</Text>
                    <View style = {props.type == 'list' ? {top: 20, flexDirection: 'row', alignItems: 'center', marginHorizontal: 10} : {}}>
                        <FontAwesome style={props.type==='list' ? {} : {opacity: 0,position: 'absolute'}} name='check-square-o' size={15} color='white'/>                                              
                        <Text numberOfLines={4} style={props.type == 'list' ? styles.todoListText :
                        styles.content}>{props.content}</Text>
                    </View>
                    <Text style={styles.date}>{props.date}</Text>
                    <View style= {styles.circle}>
                        <TouchableOpacity delayPressIn={0.4} onPress={()=> deleteNote(props.index)}>
                            <Feather name= 'x' size={30} color='white'/>
                        </TouchableOpacity>
                    </View>
            </View>
            </TouchableOpacity>
        )
    }
    return(
        <View style={styles.container}>
            <StatusBar backgroundColor='#83C0BD' barStyle='dark-content'/>
            <View style={styles.footer}>
                <TouchableOpacity delayPressIn={0.5}>
                    <Ionicons name="md-menu" size={45} color="#005669"/>
                </TouchableOpacity>
                <TouchableOpacity delayPressIn={0.5}>
                    <Ionicons name="md-search" size={40} color="#005669"/>            
                </TouchableOpacity>
            </View>
            <Text style={styles.title}>All Notes</Text>
            <FlatList
            data= {DATAS}
            keyExtractor = { item => item.key }
            renderItem = { ( {item,index} ) => (
                <BoxNotes
                content = {item.type === 'list' ? 'TODO LIST' : item.content}
                title = {item.title}
                index = {index}
                date = {item.date}
                type = {item.type}
                id = {item.key}
                key = {item.key}
                />
            )}
            />
            <View style={styles.elipsAdd}>
                <TouchableOpacity onPress={()=> navigation.push('TryPage')} delayPressIn={0.4}>
                    <Text style={styles.add}>+</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor: 'white',
    },
    footer:{
        flexDirection:'row',
        justifyContent: 'space-between',
        marginTop:15,
        marginLeft:15,
        marginRight:15,
        marginBottom: 10
    },
    title: {
        fontSize: 35,
        textAlign: "center",
        marginBottom: 15,
        fontWeight: 'bold',
        color: '#F45252'
        
    },
    boxNote: {
        backgroundColor: '#83C0BD',
        height: 140,
        marginHorizontal: 20,
        borderRadius: 18,
        marginTop: 15
    },
    titleNote: {
        marginLeft: 10,
        marginTop: 11,
        fontSize: 18,
        fontWeight: 'bold',
        color: '#044655'
    },
    content: {
        fontSize: 14,
        color: 'white',
        marginHorizontal: 10
    },
    todoListText: {
        fontSize: 15,
        color: 'white',
        marginHorizontal: 5
    },
    date: {
        fontSize: 14,
        color: 'white',
        marginLeft: 10,
        position: "absolute",
        bottom:11
    },
    elipsAdd: {
        width: 70,
        height: 70,
        backgroundColor: '#005669',
        position: 'absolute',
        borderRadius: 35,
        bottom: 48,
        right: 40
    },
    add: {
        fontSize: 50,
        color: 'white',
        textAlign: 'center'
    },
    circle: {
        backgroundColor : '#FF1F1F',
        width: 40,
        height: 40,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top : -17,
        right: 0
    }
})
