import React from 'react'
import {View, Text, TouchableOpacity, StyleSheet, StatusBar} from 'react-native';

import { Octicons } from '@expo/vector-icons';


export default function TryPage( {navigation} ){
    return(
        <View style={styles.container}>
            <StatusBar backgroundColor='#83C0BD' barStyle='light-content'/>
            <View style={styles.boxBottom}>
                <View style={styles.boxTop}>
                    <Text style={styles.title}>Select your feature!</Text>
                    <View style={{flexDirection: 'row',top:33}}>
                        <View style={styles.dot1}/>
                        <View style={styles.dot2}/>
                    </View>
                </View>
                <TouchableOpacity onPress={()=> navigation.push('BlankNote', {indexTerpilih: false} )} delayPressIn={0.4}>
                    <View style={styles.boxInternal}>
                        <Octicons style={styles.icon} name='tasklist' color='#83C0BD' size={32}/>
                        <Text style={styles.typeNote}>blank note</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=> navigation.push('CreateList', {indexTerpilih: false} )} delayPressIn={0.4}>
                    <View style={styles.boxInternal}>
                        <Octicons style={styles.icon} name='note' color='#83C0BD' size={34}/>
                        <Text style={styles.typeNote}>to do list</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E1E1E1',
        justifyContent: "center",
        alignItems: 'center'
    },
    boxTop: {
        backgroundColor: '#83C0BD',
        height: 160,
        width: 260,
        borderRadius: 35,
        marginTop: -90,
        marginBottom: 28,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.25,
        shadowRadius: 16.00,
        elevation: 10,
    },
    title: {
        fontSize: 23,
        color: '#20928C',
        textAlign: 'center',
        fontWeight: 'bold'
    },
    boxBottom: {
        backgroundColor: 'white',
        height: 340,
        paddingHorizontal: 20,
        borderRadius: 30,
        alignItems: 'center',
        shadowColor: '#000',
    },
    boxInternal: {
        backgroundColor: 'transparent',
        height: 80,
        width: 270,
        borderColor: '#D2CFCF',
        borderWidth: 4,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginVertical: 11
    },
    typeNote:{
        fontSize: 20,
    },
    icon: {
        position: 'absolute',
        left: 17
    },
    dot1: {
        width: 10,
        height: 10,
        backgroundColor: '#5CACA8',
        borderRadius: 5,
        top: 10,
        marginHorizontal: 3

    },
    dot2: {
        width: 10,
        height: 10,
        backgroundColor: 'white',
        borderRadius: 5,
        top: 10,
        marginHorizontal: 3
    }
})