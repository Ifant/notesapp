import React, { useEffect, useState } from 'react';
import { View, Text, TextInput, StyleSheet, TouchableOpacity, SafeAreaView, StatusBar, FlatList} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Ionicons } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';


let DATAS = [
    { key: null, title: '', date: '', type: '', contents: [{
        content:'',check:false
    }]}
]



export default function CreateList( {route, navigation} ) {
    const [title,setTitle] = useState('');
    const [content,setContent] = useState('');
    const [refresh,setRefresh] = useState();
    const [isEdit,setIsEdit] = useState(false);
    
    useEffect(() => {
        console.log('THIS IS USE EFFECT!');

        navigation.addListener('focus', ()=> {
            console.log('===> Masuk ke Listener');
            console.log('===> INI ROUTE BAWAAN: ',route.params.indexTerpilih)
            if (route.params.indexTerpilih === 0 || route.params.indexTerpilih){
                console.log('===> Masuk ke route params TRUE')
                setIsEdit(true);
                dataFromHome(route.params.indexTerpilih);
            }else {
                DATAS = [
                    { key: null, title: '', date: '', type: '', contents: [{
                        content:'',check:false
                    }]}
                ]
            }setRefresh( {} )
        })
    },[navigation])


    const saveToStorage = async() => {
        if (title === '' || DATAS[0].contents[0].content === '' ){
            alert('Invalid Input!');
        }else {

            if (isEdit){
                try {
                    console.log('===> Entered save storage edit');
                    let temp = JSON.parse(await AsyncStorage.getItem('allData'));
                    if (temp){
                        console.log('===> Entered temp true');
                        temp[route.params.indexTerpilih] = DATAS[0];
                        temp[route.params.indexTerpilih].title = title;
                        await AsyncStorage.setItem('allData', JSON.stringify(temp));
                    }else {
                        console.log('===> Entered temp false');
                        await AsyncStorage.setItem('allData', JSON.stringify(DATAS[0]));
                    }
                    setRefresh({});
                    navigation.navigate('Home');
                }catch (err) {
                    console.log(err);
                }
            }else {
                try{
                    console.log('===> This is SAVE TO STORAGE Non Edit')
                    console.log('DATA SEBELUM DI GET = ',temp);
                    DATAS[0].type = 'list';
                    let temp = await AsyncStorage.getItem('allData'); //ambil data dari local storage 'allData'
                    temp = JSON.parse(temp);
                    console.log('DATA SETELAH DI GET DAN PARSING = ',temp);
                    if (temp){
                        console.log('Masuk ke if!');
                        temp.push(DATAS[0]);//Jika ada, push DATAS ke allData
                        await AsyncStorage.setItem('allData',JSON.stringify(temp));
                    }else {
                        console.log('Masuk ke else!');
                        await AsyncStorage.setItem('allData',JSON.stringify(DATAS));//Selain itu, duplicate DATAS ke allData
                    }
                    DATAS = [
                        {title: '',contents: [{
                            content:'',check:false
                        }]}
                    ]
                    setTitle('');
                    setRefresh({});
                    console.log('===> DATA ALL STORAGE SETELAH DI PARSING = \n',temp);
                    navigation.navigate('Home', {isThere: true});
                }
                catch(err){
                    console.log(err);
                }
            }
        }
    }

    const dataFromHome = async(index) => {
        let temp = JSON.parse(await AsyncStorage.getItem('allData'));
        DATAS[0] = temp[index];
        setTitle(temp[index].title);
    }

    const makeDate = () => {
        let d = new Date;
        let date = d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear();
        return date;
    }

    const makeId = (length) => {//FUNCTION UNTUK MEMBUAT ID UNIQUE BERBENTUK STRING
        let result = '';
        let characters =
          'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
          result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
      }

    
    const check = (index) => {
        console.log('INI CHECK!');
        DATAS[0].contents[index].check = !DATAS[0].contents[index].check;
        setRefresh({});
    }
    
    const submitHandler = ()=> {
        console.log('SUBMIT HANDLER')
        console.log(DATAS);
        if (DATAS[0].contents[0].content==='' &&  DATAS[0].title===''){
            console.log('===> masuk ke if')
            DATAS = [];
            DATAS.push({title: title, contents: [{content: content, check: false}]});
        }else{
            console.log('===> masuk ke else!');
            DATAS[0].title = title; 
            DATAS[0].contents.push({content: content, check: false});
            // DATAS.push({content: content,check: false})
        }


        DATAS[0].key = makeId(10); //Memasukan id unique ke key
        DATAS[0].date = makeDate(); //Memasukan waktu ke object date
        setRefresh({});
        setContent('');
    }


    const ListRow = (props) => {
        console.log('=>> LIST ROW!',props.id);
        let isVisible = true;

        if (DATAS[0].contents[0].content=='' &&  DATAS[0].title==''){
            isVisible = false;
        }else{isVisible = true;}console.log('==> isVisible =',isVisible);

        return(
            <View style={isVisible ? styles.rowList : {opacity: 0}} key={props.index}>
                <TouchableOpacity onPress={()=> check(props.id)} delayPressIn={0.2} >
                    <FontAwesome name={props.check ? 'check-square-o' : 'square-o'} size={27} color='black'/>
                </TouchableOpacity>
                <Text style={styles.listText}>{props.content}</Text>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor='#83C0BD' barStyle='light-content'/>
            <View style={styles.footer}>
                <TouchableOpacity onPress={()=> navigation.goBack()} delayPressIn={0.2}>
                    <Ionicons name='ios-arrow-back' size={50} color='#005669'/>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=> saveToStorage()} delayPressIn={0.2}>
                    <View style={styles.boxSave}>
                        <Text style={styles.saveText}>save</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <SafeAreaView style={styles.container}>
                <View style= {{backgroundColor: 'white', paddingBottom: 100}}>
                    <TextInput 
                    style={styles.inputTitle}
                    placeholder='TITLE'
                    onChangeText = {teks=> setTitle(teks)}
                    value = {title}
                    />
                    
                    <FlatList
                    data={DATAS[0].contents}
                    renderItem = { ({item,index})=> (
                    <ListRow 
                    check={item.check}
                    content= {item.content}
                    id = {index}
                    key={index.toString()}
                    />
                    )}
                    keyExtractor = {item => item.content}
                    />
                </View>
            </SafeAreaView>
            
            <TextInput
            style= {styles.inputContent}
            placeholder='Tambah list baru'
            maxLength={40}
            onChangeText={teks=> setContent(teks)}
            onSubmitEditing={()=> submitHandler()}
            value = {content}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E1E1E1'
    },
    footer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 30,
        paddingHorizontal: 40,
        borderBottomWidth: 0.6,
        borderColor: '#83C0BD',
        paddingBottom: 17,
        backgroundColor: 'white'
    },
    boxSave: {
        width: 95,
        height: 50,
        backgroundColor: '#005669',
        borderRadius: 30,
    },
    saveText: {
        fontSize: 25,
        color: 'white',
        textAlign: 'center',
        marginTop: 4.5
    },
    inputTitle: {
        color: 'black',
        opacity: 0.83,
        fontSize: 29,
        fontWeight: 'bold',
        marginLeft: 40,
        marginTop: 30,
        marginBottom: 10
    },
     inputContent: {
        color: 'black',
        opacity: 0.83,
        fontSize: 20,
        paddingHorizontal: 40,
        backgroundColor: 'white',
        elevation: 10,
        position: 'absolute',
        height: 60,
        bottom: 0,
        right: 0,
        left:0
    },
    rowList: {
        marginHorizontal: 40,
        marginBottom: 10,
        flexDirection: 'row',
        alignItems: 'center',
        opacity: 0.80
    },
    listText: {
        color: 'black',
        opacity: 0.70,
        fontSize: 20,
        marginLeft: 43,
        position: 'absolute'
    }
})