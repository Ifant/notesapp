import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, StatusBar, TextInput, ScrollView } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { Ionicons } from '@expo/vector-icons';

let DATAS = [
    {key:'', title: '', content: '', date: '',type: ''}
]


export default function blankNote( {route,navigation} ){
    const [refresh,setRefresh] = useState();
    const [title,setTitle] = useState('')
    const [content,setContent] = useState('')
    const [isEdit,setIsEdit] = useState(false);

    useEffect(()=> {
        navigation.addListener('focus', ()=> {
            console.log('Masuk ke listener');
            console.log('===> Route params index = ',route.params.indexTerpilih);
            if (route.params.indexTerpilih === 0 || route.params.indexTerpilih){
                console.log('==> Masuk ke if params TRUE');
                setIsEdit(true);
                dataFromHome();
            }
            else {
                //KOSONG
            }
            setRefresh( {} )
        })
    },[navigation])

    const saveData = async()=> {
        console.log('====> ENTERED TO saveData FUNCTION ')
        if (title === '' || content === ''){
            alert('Invalid Input!');
        }else {
            if (isEdit){
                try {
                    console.log("==> Masuk ke if params")
                    let temp = JSON.parse(await AsyncStorage.getItem('allData'));
                    temp[route.params.indexTerpilih].title = title;
                    temp[route.params.indexTerpilih].content = content;
                    console.log(temp[route.params.indexTerpilih])
                    console.log("Nyampe ga ya");
                    await AsyncStorage.setItem('allData',JSON.stringify(temp));
                    setRefresh({});
                    setTitle('');
                    setContent('')
                    navigation.navigate('Home');
                }catch(err){
                    console.log(err);
                }
            }else {
                try {
                    let temp = JSON.parse(await AsyncStorage.getItem('allData'));
                    DATAS[0].title = title;
                    DATAS[0].content = content;
                    DATAS[0].date = getDate();
                    DATAS[0].key = makeId(10);
                    DATAS[0].type = 'note';
                    if (temp){
                        console.log('===> Entered If')
                        temp.push(DATAS[0])
                        await AsyncStorage.setItem('allData',JSON.stringify(temp))
                    }else {
                        console.log('===> Entered Else')
                        await AsyncStorage.setItem('allData',JSON.stringify(DATAS))
                    }
                    console.log('Menampilkan allData = ',temp);
                    setRefresh({});
                    setTitle('');
                    setContent('')
                    navigation.navigate('Home');
                }
                catch (err) { 
                    console.log(err)
                }
            }
        }
    }

    const dataFromHome = async()=> {
        try{ 
            let temp =JSON.parse(await AsyncStorage.getItem('allData'));
            DATAS[0] = temp[route.params.indexTerpilih];
            setTitle(DATAS[0].title);
            setContent(DATAS[0].content);
            setRefresh({});
        }catch(err){
            console.log(err);
        }
    }

    const getDate = ()=> {
        let d = new Date;
        let date = d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear();
        console.log('this is date ===> ',date)
        return date;
    }

    const makeId = (length) => {//FUNCTION UNTUK MEMBUAT ID UNIQUE BERBENTUK STRING
        let result = '';
        let characters =
          'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
          result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
      }

    return(
        <View style={styles.container}>
            <StatusBar backgroundColor='#83C0BD' barStyle='light-content'></StatusBar>
            <View style={styles.footer}>
                <TouchableOpacity onPress={()=> navigation.goBack()} delayPressIn={0.2}>
                    <Ionicons name='ios-arrow-back' size={50} color='#005669'/>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=> saveData()} delayPressIn={0.2}>
                    <View style={styles.boxSave}>
                        <Text style={styles.saveText}>save</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <ScrollView>
                <TextInput style= {styles.inputTitle} 
                placeholder='TITLE'
                multiline
                maxLength={44}
                onChangeText= {teks => setTitle(teks)}
                value = {title}
                />
                <TextInput style={styles.inputContent} 
                placeholder='Start to write note'
                multiline
                numberOfLines={2}
                maxLength={500}
                onChangeText= {teks => setContent(teks)}
                value = {content}
                />
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E1E1E1',
    },
    footer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 40,
        paddingHorizontal: 40
    },
    boxSave: {
        width: 95,
        height: 50,
        backgroundColor: '#005669',
        borderRadius: 30,
    },
    saveText: {
        fontSize: 25,
        color: 'white',
        textAlign: 'center',
        marginTop: 4.5
    },
    inputTitle: {
        marginTop: 80,
        marginHorizontal: 40,
        fontSize: 25,
        fontWeight: 'bold',
        opacity: 0.83
    },
    inputContent: {
        marginHorizontal: 40,
        fontSize: 17,
        color: 'black',
        opacity: 0.83
    }

})